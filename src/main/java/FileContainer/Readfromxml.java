package FileContainer;

import java.io.File;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;

public class Readfromxml {

	public static String readFromxml(String getId) {
		String id = "";
		String name = "";
		String location = "";
		String value = "";
		boolean ret = false;
		try {
			File inputFile = new File(
					"C:\\Users\\Vishal Kumar\\eclipse-workspace\\XMLEmployee.xml");
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory
					.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(inputFile);
			doc.getDocumentElement().normalize();
			System.out.println("Root element :"
					+ doc.getDocumentElement().getNodeName());
			NodeList nList = doc.getElementsByTagName("employee");
			System.out.println("----------------------------");

			for (int temp = 1; temp < nList.getLength(); temp++) {
				Node nNode = nList.item(temp);
				//System.out.println("\nCurrent Element :" + nNode.getNodeName());
				if (getId == null) {
					if (nNode.getNodeType() == Node.ELEMENT_NODE) {
						Element eElement = (Element) nNode;
						id = eElement.getElementsByTagName("id").item(0)
								.getTextContent();
						name = eElement.getElementsByTagName("name").item(0)
								.getTextContent();
						location = eElement.getElementsByTagName("location")
								.item(0).getTextContent();
						value += "<tr><td>" + id + "</td><td>" + name
								+ "</td><td>" + location + "</td></tr>";
						ret = true;
					}
				} else {
					if (nNode.getNodeType() == Node.ELEMENT_NODE) {
						Element eElement = (Element) nNode;
						id = eElement.getElementsByTagName("id").item(0)
								.getTextContent();
						if (id.equals(getId)) {
							name = eElement.getElementsByTagName("name")
									.item(0).getTextContent();
							location = eElement
									.getElementsByTagName("location").item(0)
									.getTextContent();
							ret = true;
						} else {
							continue;
						}
						if (ret)
							value = "<tr><td>" + id + "</td><td>" + name
									+ "</td><td>" + location + "</td></tr>";
						break;
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (ret)
			return value;
		else
			return "Sorry, no employee found with this id";
	}
}
