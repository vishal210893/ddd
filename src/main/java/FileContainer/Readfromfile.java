package FileContainer;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class Readfromfile {

	private static final String FILENAME = "C:\\Users\\Vishal Kumar\\eclipse-workspace\\Post.txt";

	public static String readFromFile(String id) {

		BufferedReader br = null;
		FileReader fr = null;
		String value = "";
		boolean ret = false;
		try {

			// br = new BufferedReader(new FileReader(FILENAME));
			fr = new FileReader(FILENAME);
			br = new BufferedReader(fr);
			String sCurrentLine;
			if (id != null) {
				while ((sCurrentLine = br.readLine()) != null) {
					value = sCurrentLine;
					String thisid = value.substring(0, 1);
					if (thisid.equals(id)) {
						ret = true;
						break;
					}
				}
			} else {
				while ((sCurrentLine = br.readLine()) != null) {
					value += sCurrentLine + "<br><br>";
				}
				ret = true;
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (br != null)
					br.close();
				if (fr != null)
					fr.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		if (ret)
			return value;
		else
			return "Sorry, no employee found with this id";
	}

}