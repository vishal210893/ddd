package com.vogella.jersey.todo;

import java.util.Date;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import FileContainer.Readfromfile;
import FileContainer.Readfromjson;
import FileContainer.Readfromxml;
import FileContainer.Writetofile;

@Path("/")
public class RESTfulHelloWorld {

	/*
	 * @GET
	 * 
	 * @Path("employee/{empid}")
	 * 
	 * @Produces("text/html") public Response getStartingPage(@PathParam("empid")
	 * String id) { System.out.println("id is " + id); String value =
	 * Readfromfile.readFromFile(id); String output =
	 * "<h1>Employee Detail For Emp id " + id +
	 * "<h1>ID     NAME     LOCATION<br><br>" + "<br>" + value + "</p<br>"; return
	 * Response.status(200).entity(output).build(); }
	 * 
	 * @GET
	 * 
	 * @Path("employee/")
	 * 
	 * @Produces("text/html") public Response getStartingPage() { String id = null;
	 * String value = Readfromfile.readFromFile(id); String output =
	 * "<h1>All Employee Details<h1>ID     NAME     LOCATION<br><br>" + value +
	 * "</p<br>"; return Response.status(200).entity(output).build(); }
	 * 
	 * @POST
	 * 
	 * @Path("employee/post/{post}")
	 * 
	 * @Produces("application/json")
	 * 
	 * @Consumes("application/json") public Response
	 * writeDataToFile(@PathParam("post") String post) {
	 * System.out.println("Coming inside this method"); String result = ""; boolean
	 * ret = Writetofile.writeToFile(post); if (ret) { result =
	 * "Posted Successfully"; } else { result = "Sorry Give different id"; } return
	 * Response.status(201).entity(result).build();
	 * 
	 * }
	 */

	@GET
	@Path("employee/{empid}")
	@Produces("text/html")
	public Response getStartingPage(@PathParam("empid") String id,

			@QueryParam("src") String src) {
		System.out.println("id is " + id + "     " + src);
		String value = "";
		if (src.equals("xml")) {
			System.out.println("Source is : " + src);
			value = Readfromxml.readFromxml(id);
		}
		if (src.equals("json")) {
			System.out.println("Source is : " + src);
			value = Readfromjson.readFromjson(id);
		}
		String output;
		System.out.println(value.startsWith("S"));
		if (!value.startsWith("S")) {
			output = "<h1>Employee Detail For Emp id " + id
					+ "<h1><table border='2'><tr><td>ID</td>    <td>NAME</td>     <td>LOCATION</td></tr>" + value
					+ "</table>";
		} else {
			output = "<h1>" + value + "<h1>";
		}
		return Response.status(200).entity(output).build();
	}

	@GET
	@Path("employee/")
	@Produces("text/html")
	public Response getStartingPage(@QueryParam("src") String src) {
		String id = null;
		String value = "";
		if (src.equals("xml")) {
			System.out.println("Source is : " + src);
			value = Readfromxml.readFromxml(id);
		}
		if (src.equals("json")) {
			System.out.println("Source is : " + src);
			value = Readfromjson.readFromjson(id);
		}
		String output = "<h1>All Employee Details<h1><table border='2'><tr><td>ID</td>    <td>NAME</td>     <td>LOCATION</td></tr>"
				+ value + "</table>";
		return Response.status(200).entity(output).build();
	}

}
